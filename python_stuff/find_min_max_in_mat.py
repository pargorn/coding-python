"""
This function uses Numpy (as np) to find the minimum and
maximum value in the matrix.
Created by Pargorn Puttapirat (pargorn@stu.xjtu.edu.cn) at XJTU
Created on 20180929
"""

import numpy as np


A = np.array([[0, 1], [2, 3]])

print('This is matrix A: ')
print(A)

print('\nFor column 0')
print('The min of col1 is: ' + str(np.amin(A[:, 0])))
print('The max of col1 is: ' + str(np.amax(A[:, 0])))

print('\nFor column 1')
print('The min of col1 is: ' + str(np.amin(A[:, 1])))
print('The max of col1 is: ' + str(np.amax(A[:, 1])))

