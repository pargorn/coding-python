import json

# Read the OpenHI JSON configuration file.
with open('json_read_example_file.json') as json_data_file:
    json_data = json.load(json_data_file)

# I want to know the color of the apple in the JSON object.
apple_color = json_data["apple"]["color"]

# I want to know the total amount of apple and oranges.
quantity_apple = int(json_data["apple"]["quantity"])
quantity_orange = int(json_data["orange"]["quantity"])

quantity_total = quantity_apple + quantity_orange
print('The total amount of apple and orange is : ' + str(quantity_total))

