"""
This short script will use "os" to check if the directory exists. After that it can do other appropriate action(s).
Created by Pargorn Puttapirat (pargorn@stu.xjtu.edu.cn) at XJTU
"""

import os


dir_checking = 'check_directory_result'

checking_status = os.path.exists(dir_checking)

# Print the result
print('Did the checking directory exists?: ' + str(checking_status))

if not checking_status:
    os.mkdir(dir_checking)
    print('The directory "' + dir_checking + '" did not exists and it has been created.')
else:
    print('The directory "' + dir_checking + '" already exists. No further actions are required.')

print('Done!')

